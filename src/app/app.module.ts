import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { AuthLayoutModule } from './layouts/auth-layout/auth-layout.module';
import { AdminLayoutModule } from './layouts/admin-layout/admin-layout.module';
import { ComponentsModule } from './components/components.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { DocumentviewComponent } from './pages/upload/documentview/documentview.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BankComponent } from './pages/bank/bank.component';
import { LocationStrategy, PathLocationStrategy,Location, HashLocationStrategy } from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    AdminLayoutComponent,
    DocumentviewComponent,
    BankComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    AuthLayoutModule,
    AdminLayoutModule,
    ComponentsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    BrowserAnimationsModule
    
  ],
  providers: [Location,{provide:LocationStrategy,useClass:PathLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
