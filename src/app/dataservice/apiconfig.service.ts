import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiconfigService {

  constructor(private http:HttpClient) {
    this.getConfigdata();
   }

   getConfigdata(): void {
    this.http.get<any>('assets/dodl.api.config.json').subscribe(res => {
        sessionStorage.setItem('api_url', res.apiurl);
        sessionStorage.setItem('postcode_api', res.postcode_api);
        sessionStorage.setItem('POSTCODE_KEY', res.POSTCODE_KEY);
        sessionStorage.setItem('vtiger_api', res.vtiger_api);
      }
    );
  }
}




