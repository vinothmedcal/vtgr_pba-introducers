import { Component, OnInit } from '@angular/core';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  logininfo:any;
  document_array :any;
  nodata:boolean = false;
  viewpath:any;
  constructor(private ds:DataserviceService,private router:Router,private location:Location) {
    this.viewpath= "http://casetoanswer.com/v2/storage/app/";
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'))
    // console.log('Bearer '+' '+logininfo['success'].token);
    this.getuploadelist();

   }

  ngOnInit(): void {
  }

  getuploadelist(){
    this.ds.getrecord('document','Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      console.log(res);
     let data = res['success'].data;
     let objectuploadlist = [];
     data.forEach(e => {
       let path = e.path.split('/');
       objectuploadlist.push({id:e.id,date:e.date,title:e.title,size:e.size,description:e.description,path:path[1],opath:e.path});
     })
     if(objectuploadlist.length > 0){
       this.document_array = objectuploadlist;
     }else{
      this.nodata = true;
     }
    
    })
  }

  viewdocument(data){
    const navigationExtras = {
      queryParams: {
          document_id: data      
      }
  };
this.router.navigate(['/documentview'], navigationExtras);
  
  }

  deletedocument(id){
    this.ds.deleterecord('document/'+id).subscribe(res => {
      if(res){
        Swal.fire({
          title:'Success.',
          text:res['success'].message,
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => {
          if (result.value) {
            this.document_array = [];
            this.getuploadelist();
            this.router.navigateByUrl("/upload", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
              this.router.navigateByUrl('/upload');
            });
          }
        })
        // alert(res['success'].message);
        // this.router.navigateByUrl("/cases", { skipLocationChange: true }).then(() => {
        //   console.log([decodeURI(this.location.path())]);
        //   this.router.navigate([decodeURI(this.location.path())])
        // });
      }
    })
      }



}
