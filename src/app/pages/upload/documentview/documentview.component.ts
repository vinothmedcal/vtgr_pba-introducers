import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { isEmpty } from 'rxjs/operators';
import { empty } from 'rxjs';
@Component({
  selector: 'app-documentview',
  templateUrl: './documentview.component.html',
  styleUrls: ['./documentview.component.scss']
})
export class DocumentviewComponent implements OnInit {
  logininfo:any;
  documentid:any;
  documentarraydata:any;
  nodata = false; 
  constructor(private ds:DataserviceService,private router: ActivatedRoute,private route:Router) { 
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
    this.router.queryParams.subscribe(res => {
      this.documentid = res.document_id;
    this.ds.getrecord('document/'+res.document_id,'Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      let documentdata = [];
      documentdata.push(res['success'].data);
      this.documentarraydata = documentdata;
      
    })
    });

  }

  ngOnInit(): void {
  }

  deletedocument(id){
   
    this.ds.deleterecord('document/'+id).subscribe(res => {
      if(res){
        alert(res['success'].message);
      }
    })
      }

}
