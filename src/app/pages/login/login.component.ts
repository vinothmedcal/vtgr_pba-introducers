import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup,FormBuilder,FormArray,Validators } from '@angular/forms';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import Swal from 'sweetalert2'

import { Location } from '@angular/common';
import { error } from '@angular/compiler/src/util';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  isLoading = false;
  loginForm:FormGroup;
  isSubmitted  =  false;
  ErrorMessage = "";
  constructor(private router: Router,private fb:FormBuilder,private ds:DataserviceService,private location:Location) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      username:['',Validators.required],
      password:['',Validators.required]
    })
  }
  ngOnDestroy() {
  }

  get formControls() { return this.loginForm.controls; }

  login(){
    // alert('test');
    // this.router.navigateByUrl('/dashboard');
    // console.log(this.loginForm.value);
    this.isSubmitted = true;
    if(this.loginForm.invalid){
      return;
    }
    this.isLoading =true;
    this.ds.loginpost('login',this.loginForm.value).subscribe(res => {
  
      this.isLoading =false;
      if(res['success'] == false){
        Swal.fire({
          title:res['message'],
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => {
          if (result.value) {
            this.router.navigateByUrl("/login", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
          
            });
          }
        })
// alert(res['message']);
// return;
      }else if(res['error'] == 'Unauthorised'){
        alert(res['error']);
      }else if(res['success'].success == true){
       this.ds.seesionuser_info(res);
       this.router.navigateByUrl('/cases');
      //  alert('Login Success');
     }else{
      Swal.fire({
        title:'Error.',
        text:'Please Check Your Credential!',
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.router.navigateByUrl("/login", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.router.navigate([decodeURI(this.location.path())])
        
          });
        }
      })
      //  alert('Please Check Your Credential!')
      // document.getElementById('error_login').innerHTML = res['message'];
     }
    },error => {
      if (error.status == 401)
      this.ErrorMessage = "Incorrect username or password.";
    else
      this.ErrorMessage = "Please check your network connection.";
      this.isLoading =false;
    });
    
  }



}
