import { Component, OnInit } from '@angular/core';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  inbox_array:any;
  logininfo:any;
  nodata:boolean = false;
  user_data:any;
  userdata:any;
  constructor(private ds: DataserviceService,private router:Router,private location:Location) {
        this.logininfo = JSON.parse(sessionStorage.getItem('login_details'))
    // console.log('Bearer '+' '+logininfo['success'].token);
   
    this.ds.getrecord('profile','Bearer '+' '+this.logininfo['success'].token).subscribe(res => {
    // this.getinbox_data(res['data'].id)
      this.user_data  =res['data'];
     });
     this.getuseremaildata();
    //  console.log(this.user_data);
    this.getinboxdata();
   }

  ngOnInit(): void {

  }

  getuseremaildata(){
    this.ds.getrecord('email','Bearer '+' '+this.logininfo['success'].token).subscribe(res => {
      // console.log(res);
      let userarray = [];
      let data = res['success'].data;
      data.forEach(e => {
          userarray.push({id:e.id,email:e.email});
      });
      this.userdata = userarray;
     })
  }

  getinboxdata(){
    this.ds.getrecord('inbox','Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      console.log(res);
     let data = res['success'].data;
     let objectinbox = [];
     let udata =  this.userdata;
     data.forEach(e => {
      let email = udata.filter(personObj => personObj.id == e.to);
      // console.log(email);
     
   objectinbox.push({id:e.id,date:e.date,user_email:e.user_email,to:email[0].email,subject:e.subject,message:e.message});
  
  
     })
     if(objectinbox.length > 0){
       this.inbox_array = objectinbox;
     }else{
      this.nodata = true;
     }
    })
  }

  getinbox_data(id){
    this.ds.getrecord('inbox/'+id,'Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      // console.log(res);
     let data = res['success'].data;
     let objectinbox = [];
     data.forEach(e => {
   objectinbox.push(e);
     })
     if(objectinbox.length > 0){
       this.inbox_array = objectinbox;
     }else{
      this.nodata = true;
     }
    })
  }
  editinbox(id){
    const navigationExtras = {
      queryParams: {
          inbox_id: id      
      }
  };
this.router.navigate(['/pba_introducers/addinbox'], navigationExtras);
    // this.router.navigateByUrl('/addinbox')
  }

  deleteinbox(id){
    this.ds.postRecords('inbox/'+id,this.logininfo['success'].token).subscribe(res => {
      // alert(res);
      if(res['success'].status == 'success'){
        Swal.fire({
          title:'Success.',
          text:res['success'].message,
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => {
          if (result.value) {
            this.inbox_array = [];
            this.getinboxdata();
            this.router.navigateByUrl("/pba_introducers/inbox", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
              this.router.navigateByUrl('/pba_introducers/inbox');
            });
          }
        })
        // alert(res['message']);
      }else{
        Swal.fire({
          title:'Success.',
          text:res['success'].message,
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => {
          if (result.value) {
            this.router.navigateByUrl("/pba_introducers/inbox", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
              this.router.navigateByUrl('/pba_introducers/inbox');
            });
          }
        })
      // alert(res['success'].message);
      // this.router.navigateByUrl("/inbox", { skipLocationChange: true }).then(() => {
      //   console.log([decodeURI(this.location.path())]);
      //   this.router.navigate([decodeURI(this.location.path())])
      // });
      }
    })

  }

  inbox_view(id){
    const navigationExtras = {
      queryParams: {
          inbox_id: id      
      }
  };
this.router.navigate(['/pba_introducers/inboxview'], navigationExtras);
  }

  send_view(id){
    const navigationExtras = {
      queryParams: {
          send_id: id      
      }
  };
this.router.navigate(['/pba_introducers/sendview'], navigationExtras);
  }

}
