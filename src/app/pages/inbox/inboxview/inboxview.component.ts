import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import Swal from 'sweetalert2'
import { Location } from '@angular/common';
@Component({
  selector: 'app-inboxview',
  templateUrl: './inboxview.component.html',
  styleUrls: ['./inboxview.component.scss']
})
export class InboxviewComponent implements OnInit {
  logininfo:any;
  inboxid:any;
  inboxarraydata :any;
  constructor(private ds:DataserviceService,private router: ActivatedRoute,private route:Router,private location:Location) {
    this.logininfo = JSON.parse(sessionStorage.getItem('login_details'));
    this.router.queryParams.subscribe(res => {
      this.inboxid = res.inbox_id;
    this.ds.getrecord('inbox/'+res.inbox_id,'Bearer'+' '+this.logininfo['success'].token).subscribe(res => {
      // console.log( res['success'].data);
      let inboxarray = [];
      inboxarray.push(res['success'].data);
     this.inboxarraydata = inboxarray[0];
    //  console.log(this.inboxarraydata[0].subject);
    })
    });
  
   }

  ngOnInit(): void {
  }

 
  deleteinbox(id){
    this.ds.postDelete('inbox/'+id,this.logininfo['success'].token).subscribe(res => {
      Swal.fire({
        title:'Success.',
        text:res['success'].message,
        showConfirmButton:true,
        confirmButtonText:'Ok'
      }).then((result) => {
        if (result.value) {
          this.route.navigateByUrl("/pba_introducers/cases", { skipLocationChange: true }).then(() => {
            console.log([decodeURI(this.location.path())]);
            this.route.navigate([decodeURI(this.location.path())])
            this.route.navigateByUrl('/pba_introducers/inboxview');
          });
        }
      })
      // alert(res['success'].message);
    })

  }

}
