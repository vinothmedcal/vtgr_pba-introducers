import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators,FormControl } from '@angular/forms';
import { DataserviceService } from 'src/app/dataservice/dataservice.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { error } from 'protractor';
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss']
})
export class ChangepasswordComponent implements OnInit {
  isSubmitted = false;
  changepasswordform:FormGroup;
  constructor(private ds:DataserviceService,private fb:FormBuilder,private router:Router,private location:Location) { }

  ngOnInit(): void {
    this.changepasswordform = this.fb.group({
      current:['',Validators.required],
      password:['',[Validators.required,Validators.pattern(/^(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{9,})/)]],
      password_confirmation:['',Validators.required]
    })
  }

  get formControls() { return this.changepasswordform.controls; }

  changepassword(){
    this.isSubmitted = true;
    if(this.changepasswordform.invalid){
      return;
    }
    this.ds.postRecords('password',this.changepasswordform.value).subscribe(res=>{
      if(res['validation errors']){
          Swal.fire({
            title:'Error',
            text:res['validation errors'].password,
            showConfirmButton:true,
            confirmButtonText:'Ok'
          }).then((result) => {
            if (result.value) {
              this.router.navigateByUrl("/changepassword", { skipLocationChange: true }).then(() => {
                console.log([decodeURI(this.location.path())]);
                this.router.navigate([decodeURI(this.location.path())])
                // this.changepasswordform.reset();
                this.router.navigateByUrl('/changepassword');
              });
            }
          })
        
        // alert(res['validation errors'].password);
      }else if(res['errors']){
          Swal.fire({
            title:'Error',
            text:res['errors'][0],
            showConfirmButton:true,
            confirmButtonText:'Ok'
          }).then((result) => {
            if (result.value) {
              this.router.navigateByUrl("/changepassword", { skipLocationChange: true }).then(() => {
                console.log([decodeURI(this.location.path())]);
                this.router.navigate([decodeURI(this.location.path())])
                // this.changepasswordform.reset();
                this.router.navigateByUrl('/changepassword');
              });
            }
          })
        // alert(res['errors'][0]);
      }else if(res['success'] == true){
        Swal.fire({
          title:'Success',
          text:res['message'],
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => {
          if (result.value) {
            this.router.navigateByUrl("/login", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
              this.changepasswordform.reset();
              this.router.navigateByUrl('/login');
            });
         
          }
        })
      //  alert(res['message']);
      //  this.changepasswordform.reset();
     }
    },error => {
      if(error.status == 422){
        console.log(error);
        Swal.fire({
          title:'Error',
          text:error['error']['errors']['current password'],
          showConfirmButton:true,
          confirmButtonText:'Ok'
        }).then((result) => {
          if (result.value) {
            this.router.navigateByUrl("/changepassword", { skipLocationChange: true }).then(() => {
              console.log([decodeURI(this.location.path())]);
              this.router.navigate([decodeURI(this.location.path())])
              this.changepasswordform.reset();
              this.router.navigateByUrl('/changepassword');
            });
          }
        })
      }
    })

  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
  Object.keys(formGroup.controls).forEach(field => {  //{2}
    const control = formGroup.get(field);             //{3}
    if (control instanceof FormControl) {             //{4}
      control.markAsTouched({ onlySelf: true });
    } else if (control instanceof FormGroup) {        //{5}
      this.validateAllFormFields(control);            //{6}
    }
  });
}

}
